#include "http_server.h"

http_server::http_server(int p)
{

	port = p;
	sockfd_serv = Socket(AF_INET, SOCK_STREAM, 0);	

	int optVal = 1;
	socklen_t optLen = sizeof(optVal);	
	setsockopt(sockfd_serv, SOL_SOCKET, SO_REUSEADDR, (void*) &optVal, optLen);

	bzero (&server_address, sizeof(server_address));
	server_address.sin_family      = AF_INET;
	server_address.sin_port        = htons(port);
	server_address.sin_addr.s_addr = htonl(INADDR_ANY);

	Bind (sockfd_serv, &server_address, sizeof(server_address));
	tval.tv_usec = 0;
	tval.tv_sec = TIMEOUT;

}

http_server::~http_server()
{
	int optVal = 1;
	socklen_t optLen = sizeof(optVal);	
	setsockopt(sockfd_serv, SOL_SOCKET, SO_REUSEADDR, (void*) &optVal, optLen);
}


void http_server::run()
{
	unsigned int BUFFSIZE = 1000;
	char buffer[BUFFSIZE];

	while(1)
	{
		Listen (sockfd_serv, 64);

		int conn_sockfd = Accept (sockfd_serv, NULL, NULL);
		fd_set desc_set;
		timeval wait_value = tval;

		int response_res = 0;
		while( (wait_value.tv_sec > 0 || wait_value.tv_usec > 0) && response_res == 0)
		{
			FD_ZERO(&desc_set);
			FD_SET(conn_sockfd, &desc_set);
			
			int data = Select(conn_sockfd + 1, &desc_set, NULL, NULL, &wait_value );

			while(data > 0)
			{
				Recv(conn_sockfd, buffer, BUFFSIZE, 0);
				response_res =  send_response(buffer, conn_sockfd);
				if(response_res)
					break;

				data--;
			}
		}

		Close(conn_sockfd);
	}
}

#define KEEP_ALIVE 0
#define CLOSE 1

int http_server::send_response(string query, int conn_sockfd)
{

	string response;

	stringstream sstream;
	sstream<<query;
	
	string current_string;

	bool is_get = 0;
	// bool is_host = 0;
	// bool is_con = 0;
	bool connection = KEEP_ALIVE;
	string file;
	string host;
		
	while(getline(sstream, current_string))
	{
		stringstream sstream_line;
		sstream_line<<current_string;
		string token;
		sstream_line>>token;
		if( token.compare("GET") == 0)
		{
			is_get = 1;
			sstream_line>>file;
		}

		if( token.compare("Connection:") == 0)
		{
			// is_con = 1;

			sstream_line>>token;
			if(token.compare("keep-alive"))
				connection = KEEP_ALIVE;
			else
				connection = CLOSE;
		}

		if( token.compare("Host:") == 0)
		{
			// is_host = 1;
			sstream_line>>host;
		}
	}

	if(!is_get)
	{
		send_error(conn_sockfd, NOT_IMPLEMENTED, file);
			return connection;
	}

	string host2 = host;
	std::size_t found = host.find(":");
	if (found!=std::string::npos)
		host2 = host.substr(0, found);

	string filepath = "./strony_www/" + host2 + file;
    string permitted_path = "./strony_www/" + host2;

	char buf1[PATH_MAX + 1];
	char buf2[PATH_MAX + 1];
   	realpath(filepath.c_str(), buf1);
   	realpath(permitted_path.c_str(), buf2);
   	
   	permitted_path = string(buf2);
   	string resolved_path = string(buf1);

    if(
    	resolved_path.compare(0, permitted_path.length(), 
    	permitted_path, 0, permitted_path.length() ) != 0 
       )
    {
    	send_error(conn_sockfd, NOT_PERMITTED, file);
    	return connection;
    }


	struct stat st;

	if(stat(filepath.c_str(), &st))
	{
		send_error(conn_sockfd, NOT_FOUND, file);
		return connection;
	}

	if(S_ISDIR(st.st_mode))
	{
		send_redirect(conn_sockfd, host, file);
		return connection;
	}


	std::ifstream file_stream;
	file_stream.open(filepath.c_str());

	if(file_stream.is_open())
	{
		string data((std::istreambuf_iterator<char>(file_stream)), std::istreambuf_iterator<char>());
		send_data(conn_sockfd, file, data);
		file_stream.close();
	}
	/*else
		send_error(conn_sockfd, NOT_FOUND, file);*/

	return connection;
}

void http_server::send_data(int conn_sockfd, string file, string data)
{

	string response = "";
	response += "HTTP/1.1 200 OK\n";
		
	response += ("Content-Type: " + get_content_type(file) + "\n");
	response += ("Content-Length: " + int_to_string(data.length()) + "\n");

	response += "\n";
	response += data;
	Send (conn_sockfd, response.c_str(), response.length(), 0);
	
}

void http_server::send_redirect(int conn_sockfd, string host, string file)
{
	// std::cout<<"redirecting"<<std::endl;

	string response = "HTTP/1.1 301 Moved Permanently\n";

	if(file.compare("/"))
		response += "Location: http://" + host + file + "/index.html\n";
	else
		response += "Location: http://" + host + "/index.html\n";

	Send (conn_sockfd, response.c_str(), response.length(), 0);
}

void http_server::send_error(int conn_sockfd, int eror_message_type, string file)
{
	string response = "";
	if(eror_message_type == NOT_FOUND)
	{
		// std::cout<<"Not Found"<<std::endl;
		string err_string = "404 error";
		response = "HTTP/1.1 404 Not Found\n";
		response += ("Content-Type: " + get_content_type(file) + "\n");
		response += ("Content-Length: " + int_to_string(err_string.length()) + "\n\n");
		response += err_string;
		Send (conn_sockfd, response.c_str(), response.length(), 0);
	}

	if(eror_message_type == NOT_IMPLEMENTED)
	{
		// std::cout<<"Not Implemented"<<std::endl;
		response += "HTTP/1.1 501 Not Implemented\n";
		Send (conn_sockfd, response.c_str(), response.length(), 0);
	}

	if(eror_message_type == NOT_PERMITTED)
	{
		// std::cout<<"Not Permitted"<<std::endl;
		string err_string = "not permitted";
		response = "HTTP/1.1 501 Not Found\n";
		response += ("Content-Type: " + get_content_type(file) + "\n");
		response += ("Content-Length: " + int_to_string(err_string.length()) + "\n\n");
		response += err_string;
		Send (conn_sockfd, response.c_str(), response.length(), 0);
	}

}

string http_server::get_content_type(string filename)
{
	std::size_t found;

	found = filename.find(".html");
	if (found!=std::string::npos)
		return "text/html";

	found = filename.find(".txt");
	if (found!=std::string::npos)
		return "text/plain";

	found = filename.find(".css");
	if (found!=std::string::npos)
		return "text/css";

	found = filename.find(".jpg");
	if (found!=std::string::npos)
		return "image/jpeg";

	found = filename.find(".jpeg");
	if (found!=std::string::npos)
		return "image/jpeg";

	found = filename.find(".png");
	if (found!=std::string::npos)
		return "image/png";

	found = filename.find(".pdf");
	if (found!=std::string::npos)
		return "application/pdf";

	return "application/octet-stream";


}

string http_server::int_to_string(int input)
{
	stringstream ss;
	ss<<input;
	return ss.str();
}
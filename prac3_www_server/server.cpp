#include <iostream>
#include <sstream>
#include "http_server.h"

using namespace std;

int main(int argc, char** argv)
{
	try
	{
		if(argc != 2)
		{
			cout<<"Wrong number of arguments\n";
			cout<<"USAGE: server port\n";
			return 1;
		}

		stringstream ss;
		int port;
		ss<<argv[1];
		ss>>port;
		if(port < 0)
		{
			cout<<"Bad port\n";
			return 1;
		}

		http_server server(port);
		server.run();
	}
	catch(string * str)
	{
		cout<<*str<<endl;
	}

	return 0;
}
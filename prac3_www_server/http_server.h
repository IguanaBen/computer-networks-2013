#ifndef __HTTPSERVER_H_
#define __HTTPSERVER_H_

#include <limits.h>
#include <stdlib.h>
#include <string>
#include <arpa/inet.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h> 
#include <sstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fstream>
#include "sockwrap.h"

using std::string;
using std::stringstream;

#define NOT_FOUND 0
#define NOT_IMPLEMENTED 1
#define NOT_PERMITTED 2


#define TIMEOUT 1

class http_server
{
	public:
		http_server(int p);
		~http_server();
		void run();
	private:
		string get_content_type(string filename);
		int send_response(string query, int conn_sockfd);
		int port;
		sockaddr_in server_address, client_address;
		int sockfd_serv;
		string int_to_string(int input);
		void send_error(int conn_sockfd, int eror_message_type, string file);
		void send_data(int conn_sockfd, string file, string data);
		void send_redirect(int conn_sockfd, string host, string file);
		timeval tval;
};

#endif
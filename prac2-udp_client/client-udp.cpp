#include <iostream>
#include <sstream>

#include "udp_client.h"
#include "data.h"

using namespace std;

int main(int argc, char** argv)
{
	try
	{
		if(argc != 4)
		{
			cout<<"Wrong number of arguments"<<endl;
			cout<<"USAGE: client-udp PORT output_file bytes";
			return 1;
		}

		stringstream ss;
		int port, bytes;
		string output_filename(argv[2]);
		ss<<argv[1];
		ss>>port;
		ss.str("");
		ss.clear();
		ss<<argv[3];
		ss>>bytes;

		if(port < 40001 || port > 40010)
		{
			cout<<"Wrong port"<<endl;
			return 1;
		}

		if(bytes < 0 || bytes > 1001000)
		{
			cout<<"Wrong number of bytes"<<endl;
			return 1;
		}



		udp_client client( port );
		data recieved_data = client.recieve( bytes );
		cout<<endl;
		recieved_data.write_file(output_filename);
	}
	catch(string * s)
	{
		cout<<*s<<endl;
	}

	return 0;
}
#include "request.h"

request::request(unsigned int st, unsigned int sz)
{
	start = st; size  = sz;
}

request::~request(){}

string request::get_request_string()
{
	string str_start, str_size;
	stringstream ss;
	ss<<start;
	ss>>str_start;
	ss.clear();
	ss<<size;
	ss>>str_size;
	return "GET "+ str_start + " " + str_size + "\n";
}

unsigned int request::get_start()
{
	return start;
}

unsigned int request::get_size()
{
	return size;
}
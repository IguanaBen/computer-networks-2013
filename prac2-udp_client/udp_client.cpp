#include "udp_client.h"
#include <iostream>
#include <sys/time.h>

using std::cout;

udp_client::udp_client(unsigned short int port)
{
	server_port = port;
	socket = Socket(AF_INET, SOCK_DGRAM, 0);

	bzero(&server_address, sizeof(server_address));
	server_address.sin_family      = AF_INET;
	server_address.sin_port        = htons(port);
	inet_pton(AF_INET, get_ip_from_hostname(host.c_str()).c_str(), &server_address.sin_addr);

	tval.tv_sec = 0;
	tval.tv_usec = U_SEC_WAIT_VALUE;
}

data udp_client::recieve(unsigned int bytes)
{
	queue<request> req_queue;
	map<int,bool > is_recieved;

	gen_request_queue(bytes, req_queue, is_recieved);
	char *recieve_buffer = new char[ RECIEVE_MSG_SIZE + 200 ];

	string server_ip = get_ip_from_hostname(host.c_str());

	struct sockaddr_in src_adress;
	socklen_t len = sizeof(src_adress);

	data res_data(bytes);

	int recv_bytes = 0;
	cout.precision(3);

	while( !req_queue.empty() )
	{	

		cout<<"\r";
		cout<<recv_bytes<<"B out of "<< bytes <<"B ("<< recv_bytes/ double(bytes) *100<<"%) " ;

		for(int a = 0; a < REQUEST_NUMBER && !req_queue.empty(); a++)
		{
			 request r = req_queue.front();
			 req_queue.pop();
			 if( !is_recieved[ r.get_start() ] )
			 {

			 	string req_string = r.get_request_string();
			 	Sendto(socket, req_string.c_str(), req_string.length(),
					0, &server_address, sizeof(server_address));
			 	req_queue.push(r);
			 }
		}

		FD_ZERO(&desc_set);
		FD_SET(socket,&desc_set);

		timeval wait_value = tval;
		timeval now;
		gettimeofday(&now, NULL);
		timeval end;
		timeradd(&now, &wait_value, &end);

		do
		{
			int result = Select(socket + 1, &desc_set, NULL, NULL, &wait_value);

			gettimeofday(&now, NULL);
			timersub(&end, &now ,&wait_value);
			
			if( result > 0 )
			{	
				
				int recieved_bytes = 
						Recvfrom(socket, recieve_buffer, RECIEVE_MSG_SIZE + 200, 0, &src_adress, &len);
				recieve_buffer[recieved_bytes] = 0;

				string str_src_adress = inet_ntoa(src_adress.sin_addr);
				unsigned short int src_port =  ntohs(src_adress.sin_port);
				
				if( src_port ==  server_port && !str_src_adress.compare(server_ip) )
				{
					stringstream buf_stream;

					buf_stream << recieve_buffer;
					string d;
					buf_stream>>d;
					if(!d.compare("DATA"))
					{
						unsigned int start, size;
						buf_stream >> start >> size;
						if( size == RECIEVE_MSG_SIZE || 
						   	(start == (bytes - (bytes % RECIEVE_MSG_SIZE)) &&
						    size == bytes % RECIEVE_MSG_SIZE) 
						   )
							if(!is_recieved[start])
							{	
								is_recieved[start] = true;

								char *DATA = recieve_buffer;

								while( DATA[0] != '\n' )
									DATA++;

								DATA +=1;

								recv_bytes += size;
								res_data.add_data(DATA, start, size);
							}
					}

				}
			}

		}while(wait_value.tv_sec > 0 && wait_value.tv_usec > 0);
	}

	delete[] recieve_buffer;

	return res_data;
}

void udp_client::gen_request_queue(unsigned int bytes,
									queue<request> &req_queue,
									map<int,bool> &is_recieved)
{
	is_recieved.clear();

	int b = bytes;
	for( int a = 0; a <= b - RECIEVE_MSG_SIZE; a += RECIEVE_MSG_SIZE )
	{
		req_queue.push(request(a , RECIEVE_MSG_SIZE));
		is_recieved[a] = false;
	}

	if(bytes % RECIEVE_MSG_SIZE != 0)
	{
		req_queue.push(request(bytes - (bytes % RECIEVE_MSG_SIZE), bytes % RECIEVE_MSG_SIZE));
		is_recieved[bytes - (bytes % RECIEVE_MSG_SIZE)] = false;
	}

}


string udp_client::get_ip_from_hostname(const char * hostname)
{
	// return "156.17.4.30";

    struct hostent *ho;
    struct in_addr **addr_list;
    int i;
         
    if( (ho = gethostbyname(hostname) ) == NULL) 
        throw new string("get ip error");
 
    addr_list = (struct in_addr **) ho->h_addr_list;
     
    for(i = 0; addr_list[i] != NULL; i++) 
       return inet_ntoa(*addr_list[i]);

   	throw new string("get ip error");
}
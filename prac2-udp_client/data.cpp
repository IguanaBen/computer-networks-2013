#include "data.h"

data::data(unsigned int data_size)
{
	d_size = data_size;
	d = new char[data_size];
}

data::~data()
{
	delete[] d;
}

void data::add_data(const char * r_data, unsigned int start, unsigned int size)
{
	for(unsigned int i = start; i < start + size; i++)
		d[i] = r_data[i-start];
}

void data::write_file(string filename)
{
	fstream output_stream;
	output_stream.open(filename.c_str(), ios::out | ios::binary);
	for(unsigned int i = 0; i < d_size; i++ )
		output_stream<<d[i];
	output_stream.close();
}
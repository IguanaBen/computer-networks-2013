#ifndef UDP_CLIENT_H
#define UDP_CLIENT_H 1

#include <queue>
#include <string>
#include <map>
#include <arpa/inet.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h> 
#include <sstream>

#include "sockwrap.h"
#include "data.h"
#include "request.h"

#include <stdio.h>


using std::string;
using std::queue;
using std::map;
using std::stringstream;



const string host = "aisd.ii.uni.wroc.pl";

#define RECIEVE_MSG_SIZE 1000
#define REQUEST_NUMBER 10
#define U_SEC_WAIT_VALUE 400


class udp_client
{
	private:
		string get_ip_from_hostname(const char * hostname);
		int socket;
		struct sockaddr_in server_address;
		timeval tval;
		void gen_request_queue(unsigned int bytes,
								queue<request> &req_queue,
								map<int,bool> &is_recieved);
		fd_set desc_set;
		unsigned short int server_port;

	public:
		udp_client(unsigned short int port);
		~udp_client() { };
		data recieve(unsigned int bytes);

};

#endif
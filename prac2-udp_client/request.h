#ifndef REQUEST_H
#define REQUEST_H 1

#include <string>
#include <sstream>

using std::string;
using std::stringstream;

class request
{
	private:
		unsigned int start, size;
	public:
		string get_request_string();
		request(unsigned int st, unsigned int sz);
		~request();		
		unsigned int get_size();
		unsigned int get_start();
};

#endif

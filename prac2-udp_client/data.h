#ifndef DATA_H
#define DATA_H 1

#include <string>
#include <fstream>

using std::string;
using std::fstream;
using std::ios;

class data
{
		char *d;
		unsigned int d_size;

	public:
		data(unsigned int data_size);
		~data();
		void add_data(const char * r_data, unsigned int start, unsigned int size);
		void write_file(string filename);

};

#endif
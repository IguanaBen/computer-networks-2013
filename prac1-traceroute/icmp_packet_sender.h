#ifndef ICMP_PACKET_SENDER
#define ICMP_PACKET_SENDER 1

#include <arpa/inet.h>
#include <netinet/ip_icmp.h>
#include <string>
// #include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <string.h>
#include "sockwrap.h"
#include "icmp.h"

using std::string;

class icmp_packet_sender
{
	private:
		unsigned short int last_seq;
		sockaddr_in target_adress;
		icmp icmp_packet;
		int socket;

	public:
		icmp_packet_sender(string str_adr, unsigned short int icmp_packet_id);
		~icmp_packet_sender();
		unsigned short int send_package(int TTL, timeval &send_time);
};

#endif
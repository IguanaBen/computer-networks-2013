#include "icmp_packet_sender.h"
#include "icmp_packet_reciever.h"
#include <unistd.h>
#include <iostream>

#define MAX_HOPS 30

using namespace std;

string format_adresses( string adr[3]);

int main(int argc, char** argv)
{
	if(argc != 2)
	{
		cout<<"Wrong number of arguments"<<endl;
		cout<<"USAGE: traceroute <IP>"<<endl;
		return 1;
	}

	int process_id  = getpid();
	try{

		router_data r;
		int counter = 1;
		icmp_packet_sender sender(argv[1], process_id);
		icmp_packet_reciever reciever;
		do
		{
			timeval send_time;
			unsigned short int seq = sender.send_package(counter, send_time);
			r = reciever.recieve(process_id, seq, send_time);

			cout<<counter<<". ";

			cout<<format_adresses(r.address)<<"    ";

			switch(r.address_count)	
			{
				case 0:
					cout<<"*"<<endl;
					break;
				case 3:
					cout<<r.m_time/1000.0<<" ms"<<endl;
					break;
				default:
					cout<<"???"<<endl;
					break;


			}
		counter++;

		}while( r.type != PACKET_TYPE_ECHO && counter <= MAX_HOPS);
	}
	catch(std::string *s)
	{
		cout<<*s<<endl;
	}

	return 0;
}

string format_adresses(string adr[3])
{
	// return adr[0]+ " "+ adr[1] + " "+ adr[2];
	string res = "";
	if( !adr[0].compare(adr[1]) || !adr[0].compare(adr[2]))
		adr[0] = "";

	if(!adr[1].compare(adr[2]))
		adr[1] = "";

	for (int i = 0; i < 3; ++i)
		if(adr[i].compare(""))
			res += "  " + adr[i];

	return res;
}
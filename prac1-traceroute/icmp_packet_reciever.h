#ifndef ICMP_PACKET_RECIEVER
#define ICMP_PACKET_RECIEVER 1

#include <arpa/inet.h>
#include <netinet/ip_icmp.h>
#include <string>
#include <string.h>
#include <sys/time.h>
// #include <time.h>
#include <unistd.h>
#include "sockwrap.h"
#include "icmp.h"

using std::string;

#define PACKET_TYPE_EXCEEDED 0
#define PACKET_TYPE_ECHO 1

struct router_data
{
	int address_count;
	string address[3];
	unsigned int m_time;
	bool type;
};

class icmp_packet_reciever
{
	private:
		int socket;
		fd_set desc_set;
		unsigned char buffer[IP_MAXPACKET + 1];
		timeval tval;
	public:
		icmp_packet_reciever();
		~icmp_packet_reciever();
		router_data recieve(unsigned short int icmp_packet_id,
							unsigned short int seq_id,
							timeval send_time);
};

#endif
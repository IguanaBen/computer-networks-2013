#include "icmp_packet_sender.h"

icmp_packet_sender::icmp_packet_sender(string str_adr, unsigned short int icmp_packet_id)
{
	last_seq = 0;

	bzero (&target_adress, sizeof(target_adress));
	target_adress.sin_family = AF_INET;
	if(!inet_pton(AF_INET, str_adr.c_str(), &target_adress.sin_addr))
		throw new string("Bad IP adress");

	try{
		socket = Socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
	}catch(string *s)
	{
		throw new string( *s + string(", did you run this program as root?"));
	}

	icmp_packet.icmp_type = ICMP_ECHO;
	icmp_packet.icmp_code = 0;
	icmp_packet.icmp_id = icmp_packet_id;
	icmp_packet.icmp_seq = 15;
	icmp_packet.icmp_cksum = 0;
	icmp_packet.icmp_cksum = in_cksum((u_short*)&icmp_packet, 8, 0);
}

icmp_packet_sender::~icmp_packet_sender()
{
}

unsigned short int icmp_packet_sender::send_package(int TTL, timeval &send_time)
{
	Setsockopt (socket, IPPROTO_IP, IP_TTL, &TTL, sizeof(int));
	gettimeofday(&send_time, NULL);
	for (int i = 0; i < 3; ++i)
	{	
		icmp_packet.icmp_seq = last_seq + i;
		icmp_packet.icmp_cksum = 0;
		icmp_packet.icmp_cksum = in_cksum((u_short*)&icmp_packet, 8, 0);
		Sendto(socket, &icmp_packet, ICMP_HEADER_LEN, 0, &target_adress, sizeof(target_adress));
	}
	last_seq += 3;
	return (last_seq - 3);
}
#include "icmp_packet_reciever.h"

icmp_packet_reciever::icmp_packet_reciever()
{
	try{
	socket = Socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
	}catch(string *s)
	{
		throw new string( *s  + string(", did you run this program as root?"));
	}

	tval.tv_sec = 1;
	tval.tv_usec = 0;
}

icmp_packet_reciever::~icmp_packet_reciever()
{

}

router_data icmp_packet_reciever::recieve(unsigned short int icmp_packet_id,
											unsigned short int seq_id,
											timeval send_time)	//recieve 3 packets
{
	router_data r;
	r.type = PACKET_TYPE_EXCEEDED;

	timeval start;
	timeval end;
	timeval wait_value = tval;
	gettimeofday(&start, NULL);

	timeradd(&start, &tval, &end);

	FD_ZERO(&desc_set);
	FD_SET(socket ,&desc_set);

	unsigned char* 	buffer_ptr;

	int address_count = 0;

	timeval packet_time[3];
	// bool recieved[3];

	do
	{
		if(wait_value.tv_usec <0 || wait_value.tv_sec < 0)
			break;

		int result = Select(socket + 1, &desc_set, NULL, NULL, &wait_value);
		if(result > 0 )
		{
			timeval now;
			gettimeofday(&now, NULL);
			timersub(&now, &send_time, &packet_time[address_count]);
			timersub(&end, &now ,&wait_value);

			sockaddr_in sender;
			socklen_t sender_len = sizeof(sender);
			buffer_ptr = buffer;
			Recvfrom (socket, buffer_ptr, IP_MAXPACKET, 0, &sender, &sender_len);

			ip* packet = (ip*) buffer_ptr;
			buffer_ptr += packet->ip_hl * 4;

			icmp* icmp_packet = (icmp*) buffer_ptr;
			buffer_ptr += ICMP_HEADER_LEN;

			if (icmp_packet->icmp_type == ICMP_TIME_EXCEEDED && 
				icmp_packet->icmp_code == ICMP_EXC_TTL)
			{
				
				ip* packet_orig = (ip*) buffer_ptr;
				buffer_ptr += packet->ip_hl * 4;

				icmp* icmp_packet_orig = (icmp*) buffer_ptr;

				if( packet_orig->ip_p == IPPROTO_ICMP )
				{
					unsigned short int id = icmp_packet_orig->icmp_id;
					unsigned short int seq = icmp_packet_orig->icmp_seq;

					if(id == icmp_packet_id && seq >= seq_id && seq < seq_id+3)
					{ 
						r.type = PACKET_TYPE_EXCEEDED;
						char str[20];
						inet_ntop(AF_INET, &(sender.sin_addr), str, sizeof(str));
						r.address[seq-seq_id] = string(str);
						address_count++;
						// recieved[seq-seq_id] = true;
					}
				}

			}

			if (icmp_packet->icmp_type == ICMP_ECHOREPLY)
			{
					

					unsigned short int id = icmp_packet->icmp_id;
					unsigned short int seq = icmp_packet->icmp_seq;
					if(id == icmp_packet_id && seq >= seq_id && seq < seq_id+3)
					{
						r.type = PACKET_TYPE_ECHO;
						char str[20];
						inet_ntop(AF_INET, &(sender.sin_addr), str, sizeof(str));
						r.address[seq-seq_id] = string(str);
						address_count++;
						// recieved[seq-seq_id] = true;
					}
			}

		}
		
	}while(address_count<3 && wait_value.tv_sec*1000000 + wait_value.tv_usec >0);

	usleep(wait_value.tv_usec);
	// std::cout<<r.type<<std::endl;
	if(address_count == 3)
	{
		unsigned int total_time =0;
		for (int i = 0; i < 3; ++i)
			total_time += (packet_time[i].tv_sec*1000000 + packet_time[i].tv_usec);
		r.m_time = total_time/3;
	}
	// else
		// r.m_time = 999992424;
	r.address_count = address_count;
	return r;
}